# Test

- Node.js v.14.x 〜
- yarn

- HTML5
- SCSS
- TypeScript


## Setup

```
yarn
```

```
yarn start
```


## Directory

- src(開発ディレクトリ)
- public_html(公開ディレクトリ)


## Git

### Branch
- master
- develop
- feature/
- hotfix/

### 識別子の種類
- fix：バグの修正
- add：新規機能追加・ファイルの追加
- update：バグではない機能修正
- remove：削除（ファイル）


## CSS

FLOCSSをベースとしたCSS設計を使用します。FLOCSSについては以下参照。
[FLOCSS](https://github.com/hiloki/flocss)

- setting = mixin、変数 ...
- foundation = ベース ...
- layout = ページのレイアウトを構成する共通要素（ヘッダー、サイドナビ ...）
- object > component = 最小単位のパーツ（ボタン、タイトル ...）
- object > project = componentを含むもしくはそれ以外の要素で構成されたサイト内で使い回しが効くパーツ
- object > utility = ユーティリティクラス


### EditorConfig

各エディタでプラグインをインストールしてください。
参考: https://qiita.com/naru0504/items/82f09881abaf3f4dc171